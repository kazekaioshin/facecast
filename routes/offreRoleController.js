var express = require('express');
var router = express.Router();
var offresRoles = require('../models/offresRoles');
var method_Override = require('../node_modules/method-override');
var bodyParser = require('../node_modules/body-parser');


router.use(bodyParser.urlencoded({extended:true}));

router.use(method_Override(function(req,res){
    if(req.body && typeof req.body == 'object' && '_method' in req.body)
        var method = req.body._method;
        delete req.body._method;
        return method;
}));


//recupération des ofres
router.get('/', function(req, res, next) {
    offresRoles.find({},{},function(e,docs){
    res.render('offres_roles', {
        "offreslist" : docs
        })
    }).populate("user") // fait le lien entre la clé etrangère et sa table
      .populate("event");
});


//on retrouve un event par son ID
router.get('/validation/:id', function(req, res, next) {
    var id = req.params.id;
    offresRoles.findOne({_id:id}, function(err, offre){
                if (err){
                    console.log("errr",err);
                    //return done(err, null);
                }else{
                        res.render('validationOffre', {
                            "etat" : offre.etat,
                            "nom_evenement" :offre.event.nom,
                            "role":offre.event.role_demander,
                            "id":offre._id,
                            "nom_offreur":offre.user.nom,
                            "prenom_offreur":offre.user.prenom
                        })
                }
            }).populate('user')
              .populate('event');
});

router.put('/validation/:id/update', function(req, res, next){
    var id = req.params.id; // recupere le parametre "id" de l'url
    var choix = req.body.choix; // recupere la value de mon boutton radio 
    offresRoles.findOne({_id:id}, function(err,offre){ 
        if(err){
            res.send(err);
        }else{
            offre.etat = choix;
            offre.save(function(err,saveoffer){
                if(err){
                    res.send(err);
                }else{
                    res.redirect('/offres_roles');
                }
            })
        }

    })

});

/*
router.get('/add', function(req, res, next){
var event = "58d150d56ed1111c601d6dc4";
var user = "58d1513d6ed1111c601d6dc7";

 var newOffre = new offresRoles({
        "event" : event,
        "user" : user,
    });

    newOffre.save( function (err, doc) {
        if (err) {
            // Retour d'une erreur
            res.send("error");
        }
        else {
            // Redirection vers la liste
            res.redirect("/offres_roles");
        }
    });
});
*/

module.exports = router;


