var express = require('express');
var router = express.Router();
var events = require('../models/events');

/* Liste des evenements */
router.get('/events', function(req, res, next) {
  var response ={};
    events.find({},{},function(err,event){
      if(err){
        response = {"error" : true,"message" : "Error fetching data"};
      }else{
        response= {event};
      }
    res.json(response);
  });
});

module.exports = router;
