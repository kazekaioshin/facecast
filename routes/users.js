var express = require('express');
var router = express.Router();
var users = require('../models/users');

/* GET users listing. 
router.get('/', function(req, res, next) {
  res.render('users');
}); */


/* Liste des utilisateurs */
router.get('/', function(req, res, next) {
	users.find({},{},function(e,docs){
	res.render('users', {
		"userslist" : docs
		});
	});
});

/*formulaire d'ajout utilisateur */
router.get('/insert', function(req, res, next) {
  res.render('insertUsers');
});

/* Ajout d'un utilisateur*/
router.post('/insert/addevent', function(req, res) {
    // Récupération des valeurs du formulaire
    var userName = req.body.userName;
    var userFirstName = req.body.userFirstName;
    var userSexe = req.body.userSexe;
    var userDate = req.body.userDate;

    // Création de l'objet event suivant le schéma
    var newUser = new users({
        "nom" : userName,
        "prenom" : userFirstName,
        "sexe" : userSexe,
        "date_naissance" : userDate
    });

    newUser.save( function (err, doc) {
        if (err) {
            // Retour d'une erreur
            res.send("error");
        }
        else {
            // Redirection vers la liste
            res.redirect("/users");
        }
    });
});

module.exports = router;
