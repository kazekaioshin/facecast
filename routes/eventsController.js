var express = require('express');
var router = express.Router();
var events = require('../models/events');
var offresRoles = require('../models/offresRoles')

var method_Override = require('../node_modules/method-override');
var bodyParser = require('../node_modules/body-parser');

router.use(bodyParser.urlencoded({extended:true}));

router.use(method_Override(function(req,res){
    if(req.body && typeof req.body == 'object' && '_method' in req.body)
        var method = req.body._method;
        delete req.body._method;
        return method;
}));


/* Liste des evenements */
router.get('/', function(req, res, next) {
	events.find({},{},function(e,docs){
	res.render('events', {
		"eventslist" : docs
		});
	});
});

/*formulaire d'ajout evenement */
router.get('/insert', function(req, res, next) {
  res.render('insertEvents');
});


/* Ajout d'un evenement*/
router.post('/insert/addevent', function(req, res) {
    // Récupération des valeurs du formulaire
    var eventName = req.body.eventName;
    var eventType = req.body.eventType;
    var eventDate = req.body.eventDateTournage;
    var eventRole = req.body.eventRole;
    var eventNbRole = req.body.eventNbRole;


    // Création de l'objet event suivant le schéma
    var newEvent = new events({
        "nom" : eventName,
        "type" : eventType,
        "date_tournage" : eventDate,
        "role_demander" : eventRole,
        "nb_role" : eventNbRole
    });

    newEvent.save( function (err, doc) {
        if (err) {
            // Retour d'une erreur
            res.send("error");
        }
        else {
            // Redirection vers la liste
            res.redirect("/events");
        }
    });
});

router.get('/:id', function(req, res, next) {
    var id = req.params.id;
    events.findOne({_id:id}, function(err, event){
                if (err){
                    console.log("errr",err);
                    //return done(err, null);
                }else{
                        res.render('remove', {
                            "id" : event._id,
                            "nom_evenement" : event.nom,             
                        })
                }
            });
});



router.delete('/:id/delete',function(req,res,next){
    var id =req.params.id;
    events.findOneAndRemove({_id:id},function(err){
        if(err){
            res.send(err);
        }else{
            offresRoles.find({event:id}).remove().exec();
            res.redirect("/events");
        }
    });
})


module.exports = router;
