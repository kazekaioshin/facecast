var mongoose = require('mongoose');

// Création du schéma pour les commentaires
var eventsSchema = new mongoose.Schema({
  nom : String,
  type : String,
  role_demander : String,
  nb_role : String,
  date_tournage : String
});

module.exports  = mongoose.model('events', eventsSchema,'events');

