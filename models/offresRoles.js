var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// Création du schéma pour les commentaires
var offreRoleSchema = new mongoose.Schema({
  etat : {type : String , default: 'en attente'},
  user : { type: Schema.Types.ObjectId, ref: 'users' },
  event : { type: Schema.Types.ObjectId, ref: 'events' }
});

module.exports  = mongoose.model('offreRole', offreRoleSchema,'offreRole');

