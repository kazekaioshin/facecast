var mongoose = require('mongoose');

// Création du schéma pour les commentaires
var usersSchema = new mongoose.Schema({
  nom : String,
  prenom : String,
  sexe : String,
  date_naissance : String
});

module.exports  = mongoose.model('users', usersSchema,'users');